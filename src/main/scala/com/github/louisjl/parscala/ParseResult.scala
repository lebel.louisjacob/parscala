package com.github.louisjl.parscala

final case class ParseResult[S, +A](source: S, data: A)

object ParseResult {
    type Either[S, +A] = ParseException.Either[ParseResult[S, A]]
}
