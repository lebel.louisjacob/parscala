package com.github.louisjl.parscala

import scala.collection.generic.CanBuildFrom

sealed trait DefaultSourceAux[S] {
    def empty: S
}

object DefaultSourceAux {
    implicit def canBuildFromDefaultSource[A, R](implicit cbf: CanBuildFrom[R, A, R]): DefaultSourceAux[R] = new DefaultSourceAux[R] {
        override def empty: R = cbf().result()
    }

    implicit def unitDefaultSource: DefaultSourceAux[Unit] = new DefaultSourceAux[Unit] {
        override def empty: Unit = ()
    }
}
