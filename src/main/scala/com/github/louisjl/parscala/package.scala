package com.github.louisjl

import com.github.louisjl.parscala.Parser._
import com.github.louisjl.util.EitherExceptionOps

import scala.language.implicitConversions

package object parscala {
    implicit final class ParserOps[P, S, I, O](self: P)(implicit ev: P => Parser[S, I, O]) {
        def ~[C](that: Parser[S, O, C]): Parser[S, I, C] =
            new AndThenParser(self, that)

        def |[I2 <: I, O2 >: O](that: Parser[S, I2, O2]): EitherParser[S, I2, O2] =
            new EitherParser(self, that)

        def ?[I2 <: I, O2 >: O](implicit aux: ReductionAux[I2, O2]): Parser[S, O2, O2] =
            new OptionParser(aux.toReduction(self))

        def +[I2 <: I, O2 >: O](implicit aux: ReductionAux[I2, O2], sourceAux: SourceAux[O2]): Parser[S, O2, O2] =
            (1 until Int.MaxValue).times(self)

        def *[I2 <: I, O2 >: O](implicit aux: ReductionAux[I2, O2], sourceAux: SourceAux[O2]): Parser[S, O2, O2] =
            (0 until Int.MaxValue).times(self)

        def ! : Parser[S, I, Unit] =
            new NegationParser(self)

        def map[A](mapper: O => A): Parser[S, I, A] =
            new MapParser(self, mapper)

        def compose[A](composer: A => I): Parser[S, A, O] =
            new ComposeParser(self, composer)

        def parse[I2 <: I](source: S)(implicit aux: DefaultSourceAux[I2]): O = {
            val input = ParseResult(source, aux.empty)
            val result = self.tryParse(input)
            result.getRightOrThrowLeft.data
        }
    }

    def take[S: SourceAux](length: Int): Parser[S, Unit, S] =
        new LengthParser(length)

    def drop[P, S, I](self: P)(implicit ev: P => Parser[S, I, _]): Parser[S, I, Unit] =
        new DropParser(self)

    def empty[S](implicit aux: DefaultSourceAux[S]): Parser[S, Unit, S] =
        push[S, S](aux.empty)

    def push[S, A](value: A): Parser[S, Unit, A] =
        new PushParser(value)

    implicit def literally[S: SourceAux](source: S): Parser[S, Unit, S] =
        new LiteralParser(source)

    implicit def toSequent[S, L, R, O](self: Parser[S, Unit, R])(implicit combiner: Combiner[L, R, O]): Parser[S, L, O] =
        new SequentParser(self)

    implicit def fromReduction[S, I, O](self: Parser[S, I, O])(implicit aux: DefaultSourceAux[I]): Parser[S, Unit, O] =
        push[S, I](aux.empty) ~ self

    implicit class RangeOps[R](range: R)(implicit ev: R => ZeroOrMoreParser.RangeLike) {
        def times[S, I, O, I2 <: I, O2 >: O](self: Parser[S, I, O])(implicit aux: ReductionAux[I2, O2], sourceAux: SourceAux[O2]): Parser[S, O2, O2] = {
            new ZeroOrMoreParser(aux.toReduction(self), range)
        }
    }

    implicit def number2RangeLike(range: Int): ZeroOrMoreParser.RangeLike =
        range :: Nil
}
