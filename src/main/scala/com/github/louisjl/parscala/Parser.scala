package com.github.louisjl.parscala

import com.github.louisjl.parscala.ParseResult.Either
import com.github.louisjl.util._
import com.github.louisjl.util.strings.Formattable

import scala.collection.SeqLike

sealed trait Parser[S, -I, +O] extends Formattable {
    def tryParse[I2 <: I](input: ParseResult[S, I2]): ParseResult.Either[S, O]
}

object Parser {
    final class AndThenParser[S, -I, A, +O](left: Parser[S, I, A], right: Parser[S, A, O]) extends Parser[S, I, O] {
        override def tryParse[I2 <: I](input: ParseResult[S, I2]): ParseResult.Either[S, O] = {
            val leftResult = left.tryParse(input)
            val rightResult = leftResult.flatMap(right.tryParse)
            rightResult
        }

        override def format: String =
            s"$left ~ $right"
    }

    final class EitherParser[S, -I, +O](left: Parser[S, I, O], right: Parser[S, I, O]) extends Parser[S, I, O] {
        override def tryParse[I2 <: I](input: ParseResult[S, I2]): ParseResult.Either[S, O] = {
            val leftResult = left.tryParse(input)
            val rightResult = leftResult.right.orElse(right.tryParse(input))
            rightResult.right.orElse {
                val exception = new ParseException(input.source, this.format)
                Left(exception)
            }
        }

        override def format: String =
            s"$left | $right"
    }

    final class ZeroOrMoreParser[S, O, R](self: Parser[S, O, O], range: R)(implicit ev: R => ZeroOrMoreParser.RangeLike) extends Parser[S, O, O] {
        override def tryParse[I2 <: O](input: ParseResult[S, I2]): ParseResult.Either[S, O] =
            tryParseRecursive(input, 0)

        @scala.annotation.tailrec
        private def tryParseRecursive[I2 <: O](input: ParseResult[S, O], loops: Int): ParseResult.Either[S, O] = {
            val intermediateResult = self.tryParse(input)
            if (intermediateResult.isLeft)
                terminateLoop(input, loops)
            else {
                val intermediateInput = intermediateResult.right.get
                tryParseRecursive(intermediateInput, loops + 1)
            }
        }

        private def terminateLoop(input: ParseResult[S, O], loops: Int): ParseResult.Either[S, O] =
            if (range.contains(loops))
                Right(input)
            else {
                val exception = new ParseException(input.source, this.format)
                Left(exception)
            }

        override def format: String =
            s"[$range] times ($self)"
    }

    object ZeroOrMoreParser {
        type RangeLike = SeqLike[Int, _ >: Nothing]
    }

    final class OptionParser[S, O](self: Parser[S, O, O]) extends Parser[S, O, O] {
        override def tryParse[I2 <: O](input: ParseResult[S, I2]): ParseResult.Either[S, O] = {
            val selfResult = self.tryParse(input)
                selfResult.right.orElse(Right(input))
        }

        override def format: String =
            s"($self).?"
    }

    final class SequentParser[S, L, R, C](self: Parser[S, Unit, R])(implicit combiner: Combiner[L, R, C]) extends Parser[S, L, C] {
        override def tryParse[I2 <: L](input: ParseResult[S, I2]): ParseResult.Either[S, C] = {
            val selfResult = self.tryParse(ParseResult(input.source, ()))
            val combinedResult = selfResult.flatMap(selfResult => {
                val combinedData = combiner.combine(input.data, selfResult.data)
                combinedData.map(combinedData => ParseResult(selfResult.source, combinedData))})
            combinedResult
        }

        override def format: String =
            s"$self"
    }

    final class LiteralParser[S](sourceSlice: S)(implicit matcher: SourceAux[S]) extends Parser[S, Unit, S] {
        override def tryParse[I2 <: Unit](input: ParseResult[S, I2]): ParseResult.Either[S, S] = {
            val matchResult = matcher.tryRemoveStart(sourceSlice, input.source)
            matchResult.map(matchResult => ParseResult(matchResult, sourceSlice))
        }

        override def format: String =
            "\"" + s"$sourceSlice" + "\""
    }

    final class LengthParser[S](length: Int)(implicit matcher: SourceAux[S]) extends Parser[S, Unit, S] {
        override def tryParse[I2 <: Unit](input: ParseResult[S, I2]): ParseResult.Either[S, S] = {
            val matchResult = matcher.matchLength(input.source, length)
            matchResult.map(matchResult => ParseResult(matchResult._2, matchResult._1))
        }

        override def format: String =
            s"$length"
    }

    final class NegationParser[S, I](self: Parser[S, I, _]) extends Parser[S, I, Unit] {
        override def tryParse[I2 <: I](input: ParseResult[S, I2]): ParseResult.Either[S, Unit] = {
            val selfResult = self.tryParse(input)
            if (selfResult.isLeft)
                Right(ParseResult(input.source, ()))
            else
                Left(new ParseException(input.source, this.format))
        }

        override def format: String =
            s"($self).!"
    }

    final class DropParser[S, I](self: Parser[S, I, _]) extends Parser[S, I, Unit] {
        override def tryParse[I2 <: I](input: ParseResult[S, I2]): ParseResult.Either[S, Unit] = {
            val selfResult = self.tryParse(input)
            selfResult.map(selfResult => ParseResult(selfResult.source, ()))
        }

        override def format: String =
            s"$self"
    }

    final class PushParser[S, A](value: A) extends Parser[S, Unit, A] {
        override def tryParse[I2 <: Unit](input: ParseResult[S, I2]): ParseResult.Either[S, A] =
            Right(ParseResult(input.source, value))

        override def format: String =
            "<push>"
    }

    final class MapParser[S, I, O, A](self: Parser[S, I, O], mapper: O => A) extends Parser[S, I, A] {
        override def tryParse[I2 <: I](input: ParseResult[S, I2]): Either[S, A] = {
            val selfResult = self.tryParse(input)
            val mappedResult = selfResult.map(selfResult => mapper(selfResult.data))
            mappedResult.map(ParseResult(input.source, _))
        }

        override def format: String =
            s"$self"
    }

    final class ComposeParser[S, I, O, A](self: Parser[S, I, O], composer: A => I) extends Parser[S, A, O] {
        override def tryParse[I2 <: A](input: ParseResult[S, I2]): Either[S, O] = {
            val composedData = composer(input.data)
            val composedInput = ParseResult(input.source, composedData)
            self.tryParse(composedInput)
        }

        override def format: String =
            s"$self"
    }
}
