package com.github.louisjl.parscala

class ParseException(private val source: Any, val message: String) extends RuntimeException (
    s"""source did not match:
       |$message
       |$source
       |""".stripMargin
)

object ParseException {
    type Either[+A] = scala.Either[ParseException, A]
}
